﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            testEntities test = new testEntities();
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder(test.Database.Connection.ConnectionString);
            csb.CurrentLanguage = "english"; // = "eesti" - proovi
            test.Database.Connection.ConnectionString = csb.ToString();

            test.TEstName.ToList().ForEach(
                x =>
                {
                    Console.WriteLine($"{x.id} - {x.name}");
                }
                );

        }
    }
}
