create database test
go
use test
go
create table TestName
(
	id int identity primary key,
	nameEST nvarchar(30),
	nameENG nvarchar(30),
	name as 
		case 
			when @@language = 'eesti' then nameEST
			else nameENG
		end 
)

insert TestNAme values
('nimi', 'name'),
('linn', 'city')
go

set language eesti
select id, name from TestName
go
set language english
select id, name from TestName

set language english
select @@LANGUAGE
